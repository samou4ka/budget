package com.dsytnykov.budget.service;

import com.dsytnykov.budget.model.Currency;
import com.dsytnykov.budget.repository.CurrencyRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
class CurrencyServiceTest {

    @Autowired
    private CurrencyService currencyService;

    @Mock
    private CurrencyRepository currencyRepository;

    @Test
    void getAll() {
        when(currencyRepository.findAll()).thenReturn(List.of(
                Currency.builder().id(0).defaultCurrency(false).rate("1.1").symbol("USD").build(),
                Currency.builder().id(1).defaultCurrency(true).rate("1").symbol("EUR").build()));

        List<Currency> currencies = currencyService.getAll();

        assertEquals(2, currencies.size());
    }

}
