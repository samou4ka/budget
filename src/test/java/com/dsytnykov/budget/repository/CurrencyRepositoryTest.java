package com.dsytnykov.budget.repository;

import com.dsytnykov.budget.model.Currency;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
class CurrencyRepositoryTest {

    private final CurrencyRepository currencyRepository;

    public CurrencyRepositoryTest(@Autowired CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Test
    void getAll() {
        List<Currency> currencies = currencyRepository.findAll();

        assertEquals(2, currencies.size());
    }

}
