package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.CategoryDto;
import com.dsytnykov.budget.model.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
class CategoryControllerTest {

    @Autowired
    private CategoryController categoryController;

    @Test
    void getAll() {
        List<Category> categories = categoryController.getAll();

        assertEquals(3, categories.size());
    }

    @Test
    @Transactional
    void create() {
        CategoryDto category = new CategoryDto(0, "Wohnung", 0L);

        Category createdCategory = categoryController.create(category);

        assertEquals("Wohnung", createdCategory.getName());
    }

    @Test
    @Transactional
    void delete() {
        categoryController.delete(3);

        List<Category> categories = categoryController.getAll();
        assertEquals(2, categories.size());
    }

    @Test
    void findById() {
        Category category = categoryController.findById(2);
        assertEquals("test2", category.getName());
    }

    @Test
    @Transactional
    void update() {
        CategoryDto dto = new CategoryDto(1, "temp", 0L);

        Category updatedCategory = categoryController.update(dto);

        assertEquals("temp", updatedCategory.getName());
    }
}
