package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.CurrencyDto;
import com.dsytnykov.budget.model.Currency;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
class CurrencyControllerTest {

    @Autowired
    private CurrencyController currencyController;

    @Test
    void getAll() {
        List<Currency> currencies = currencyController.getAll();

        assertEquals(2, currencies.size());
    }

    @Test
    @Transactional
    void create() {
        CurrencyDto dto = new CurrencyDto(0, "UAH", "20", false);

        Currency createdCurrency = currencyController.create(dto);

        assertEquals("UAH", createdCurrency.getSymbol());
        assertEquals("20", createdCurrency.getRate());
    }

    @Test
    @Transactional
    void update() {
        CurrencyDto dto = new CurrencyDto(1, "USD", "20", false);

        Currency updatedCurrency = currencyController.update(dto);

        assertEquals("USD", updatedCurrency.getSymbol());
        assertEquals("20", updatedCurrency.getRate());
    }
}
