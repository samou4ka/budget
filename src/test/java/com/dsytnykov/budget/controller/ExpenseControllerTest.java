package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.ExpenseDto;
import com.dsytnykov.budget.model.Account;
import com.dsytnykov.budget.model.Category;
import com.dsytnykov.budget.model.Currency;
import com.dsytnykov.budget.model.Expense;
import com.dsytnykov.budget.model.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
class ExpenseControllerTest {
    @Autowired
    private ExpenseController expenseController;

    @Test
    void getAll() {
        List<Expense> expenses = expenseController.getAll();

        assertEquals(2, expenses.size());
    }

    @Test
    void getById() {
        Expense expenses = expenseController.findById(1);

        assertEquals(200, expenses.getPrice());
    }

    @Test
    void getAllByAccount() {
        List<Expense> expenses = expenseController.findByAccount(1);

        assertEquals(2, expenses.size());
    }

    @Test
    void getAllByCategory() {
        List<Expense> expenses = expenseController.findByCategory(1);

        assertEquals(1, expenses.size());
    }

    @Test
    @Transactional
    void create() {
        Currency currency = Currency.builder().id(1).defaultCurrency(true).rate("1").symbol("EUR").build();
        Category category = Category.builder().id(1).parentId(0L).name("test1").build();
        Account account = Account.builder().id(1).balance(1000).name("test").description("test").currency(currency).build();
        Payment payment = Payment.builder().id(1).name("Bar").build();

        ExpenseDto dto = new ExpenseDto(0, "test", -150, currency, category, account, payment);

        Expense returnedExpense = expenseController.create(dto);

        assertEquals(dto.price(), returnedExpense.getPrice());
        assertEquals(850, returnedExpense.getAccount().getBalance());
    }

    @Test
    @Transactional
    void delete() {

        expenseController.delete(1);

        List<Expense> expenses = expenseController.getAll();
        assertEquals(1, expenses.size());
    }

    @Test
    @Transactional
    void updateWithoutBalance() {
        Currency currency = Currency.builder().id(1).defaultCurrency(true).rate("1").symbol("EUR").build();
        Category category = Category.builder().id(1).parentId(0L).name("test1").build();
        Account account = Account.builder().id(1).balance(1000).name("test").description("test").currency(currency).build();
        Payment payment = Payment.builder().id(1).name("Bar").build();

        ExpenseDto dto = new ExpenseDto(0, "test", -150, currency, category, account, payment);

        Expense returnedExpense = expenseController.update(dto, "");

        assertEquals(dto.price(), returnedExpense.getPrice());
        assertEquals(1000, returnedExpense.getAccount().getBalance());
    }

    @Test
    @Transactional
    void updateWithBalance() {
        Currency currency = Currency.builder().id(1).defaultCurrency(true).rate("1").symbol("EUR").build();
        Category category = Category.builder().id(1).parentId(0L).name("test1").build();
        Account account = Account.builder().id(1).balance(1000).name("test").description("test").currency(currency).build();
        Payment payment = Payment.builder().id(1).name("Bar").build();

        ExpenseDto dto = new ExpenseDto(0, "test", -150, currency, category, account, payment);

        Expense returnedExpense = expenseController.update(dto, "balance");

        assertEquals(dto.price(), returnedExpense.getPrice());
        assertEquals(850, returnedExpense.getAccount().getBalance());
    }
}
