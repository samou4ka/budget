package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.PaymentDto;
import com.dsytnykov.budget.model.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
class PaymentControllerTest {

    @Autowired
    private PaymentController paymentController;

    @Test
    void getAll() {
        List<Payment> payments = paymentController.getAll();

        assertEquals(1, payments.size());
    }

    @Test
    @Transactional
    void create() {
        PaymentDto dto = new PaymentDto(0, "Credit Card");
        Payment returnedPayment = paymentController.create(dto);

        assertEquals("Credit Card", returnedPayment.getName());
    }
}
