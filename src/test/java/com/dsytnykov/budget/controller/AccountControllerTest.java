package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.AccountDto;
import com.dsytnykov.budget.model.Account;
import com.dsytnykov.budget.model.Currency;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
class AccountControllerTest {

    @Autowired
    private AccountController accountController;

    @Test
    void getAll() {
        List<Account> accounts = accountController.getAll();

        assertEquals(2, accounts.size());
    }

    @Test
    @Transactional
    void create() {
        Currency currency = Currency.builder().id(1).defaultCurrency(true).rate("1").symbol("EUR").build();
        AccountDto account = new AccountDto(2, "Credit Card", 8000, "description", currency);

        Account createdAccount = accountController.create(account);

        assertEquals("Credit Card", createdAccount.getName());
    }

    @Test
    @Transactional
    void delete() {
        accountController.delete(2);

        List<Account> accounts = accountController.getAll();
        assertEquals(1, accounts.size());
    }

    @Test
    @Transactional
    void update() {
        Currency currency = Currency.builder().id(1).defaultCurrency(true).rate("1").symbol("EUR").build();
        AccountDto account = new AccountDto(1, "temp", 8000, "description", currency);

        Account updatedAccount = accountController.update(account);

        assertEquals("temp", updatedAccount.getName());
    }
}