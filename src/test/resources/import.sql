INSERT INTO currencies (id, default_currency, rate, symbol) values (0, false, '1.1', 'USD');
INSERT INTO currencies (id, default_currency, rate, symbol) values (1, true, '1', 'EUR');

INSERT INTO accounts (id, balance, currency_id, description, name, create_date, update_date) values (1, 1000, 1, 'test', 'test', '2025-01-07 15:23:29', '2025-01-07 15:23:29' );
INSERT INTO accounts (id, balance, currency_id, description, name, create_date, update_date) values (2, 2000, 1, 'test', 'test1', '2025-01-07 15:23:29', '2025-01-07 15:23:29' );

INSERT INTO categories (id, name, parent_id) values (1, 'test1', '0');
INSERT INTO categories (id, name, parent_id) values (2, 'test2', '0');
INSERT INTO categories (id, name, parent_id) values (3, 'test3', '0');

INSERT INTO payments (id, name) values (1, 'Bar');

INSERT INTO expenses (id, price, category_id, currency_id, accound_id, comment) values (0, 100, 1, 1, 1, 'first');
INSERT INTO expenses (id, price, category_id, currency_id, accound_id, comment) values (1, 200, 2, 1, 1, 'second');

