package com.dsytnykov.budget.service;

import com.dsytnykov.budget.dto.CurrencyDto;
import com.dsytnykov.budget.model.Currency;
import com.dsytnykov.budget.repository.CurrencyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CurrencyService {

    private final CurrencyRepository repository;

    public CurrencyService(CurrencyRepository repository) {
        this.repository = repository;
    }

    public List<Currency> getAll() {
        return repository.findAll();
    }

    public Currency create(CurrencyDto dto) {
        Currency currency = new Currency();
        currency.setSymbol(dto.symbol());
        currency.setRate(dto.rate());
        currency.setDefaultCurrency(dto.defaultCurrency());

        return repository.save(currency);
    }

    public Currency update(CurrencyDto dto) {
        Optional<Currency> opt = repository.findById(dto.id());
        if(opt.isEmpty()) {
            throw new IllegalArgumentException("Currency with id " + dto.id() + " not found!");
        }
        Currency currency = opt.get();
        currency.setSymbol(dto.symbol());
        currency.setRate(dto.rate());
        currency.setDefaultCurrency(dto.defaultCurrency());

        return repository.save(currency);
    }
}
