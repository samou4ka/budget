package com.dsytnykov.budget.service;

import com.dsytnykov.budget.dto.ExpenseDto;
import com.dsytnykov.budget.model.Account;
import com.dsytnykov.budget.model.Expense;
import com.dsytnykov.budget.repository.ExpenseRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ExpenseService {

    private final ExpenseRepository repository;
    private final AccountService accountService;
    private final ExpenseRepository expenseRepository;

    public ExpenseService(ExpenseRepository repository, AccountService accountService, ExpenseRepository expenseRepository) {
        this.repository = repository;
        this.accountService = accountService;
        this.expenseRepository = expenseRepository;
    }

    public List<Expense> getAll() {
        return repository.findAll();
    }

    /**
     * Create new expense
     * @param data Expense
     * @return Expense
     */

    public Expense create(ExpenseDto data) {
        Expense expense = Expense.builder().comment(data.comment()).price(data.price()).currency(data.currency()).category(data.category()).account(data.account()).payment(data.payment()).build();
        expense.setAccount(accountService.updateAccountBalance(expense));
        return repository.save(expense);
    }

    public Expense update(ExpenseDto data, String method) throws NoSuchElementException {
        Optional<Expense> expenseFromDB = expenseRepository.findById(data.id());
        if(expenseFromDB.isEmpty()){
            throw new NoSuchElementException("Expense with id " + data.id() + " not found!");
        }
        Expense expense = expenseFromDB.get();
        expense.setAccount(data.account());
        expense.setPayment(data.payment());
        expense.setCategory(data.category());
        expense.setCurrency(data.currency());
        expense.setComment(data.comment());
        expense.setPrice(data.price());
        if("balance".equals(method)) {
            Account account = accountService.updateAccountBalance(expense);
            expense.setAccount(account);
        }

        return repository.save(expense);
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public Expense findById(long id) {
        return repository.findById(id).orElse(new Expense());
    }

    public List<Expense> findByCategoryId(long id) {
        return repository.findByCategoryId(id);
    }

    public List<Expense> findByAccountId(long id) {
        return repository.findByAccountId(id);
    }
}
