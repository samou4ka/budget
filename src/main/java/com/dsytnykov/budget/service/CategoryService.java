package com.dsytnykov.budget.service;

import com.dsytnykov.budget.dto.CategoryDto;
import com.dsytnykov.budget.model.Category;
import com.dsytnykov.budget.repository.CategoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CategoryService {

    CategoryRepository repository;

    public CategoryService(CategoryRepository repository) {
        this.repository = repository;
    }

    public Category create(CategoryDto data) {
        log.debug("Create object");
        Category category = Category.builder().name(data.name()).parentId(data.parentId()).build();
        return repository.save(category);
    }

    public Category update(CategoryDto data) {
        Category category = Category.builder().id(data.id()).name(data.name()).parentId(data.parentId()).build();
        return repository.save(category);
    }

    public List<Category> getAll() {
        return repository.findAll();
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public Category findById(long id) {
        log.debug("Get object by id");
        return repository.findById(id).orElse(new Category());
    }
}
