package com.dsytnykov.budget.service;

import com.dsytnykov.budget.dto.PaymentDto;
import com.dsytnykov.budget.model.Payment;
import com.dsytnykov.budget.repository.PaymentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    private final PaymentRepository repository;

    public PaymentService(PaymentRepository repository) {
        this.repository = repository;
    }

    public Payment create(PaymentDto data) {
        Payment payment = Payment.builder().id(data.id()).name(data.name()).build();
        return repository.save(payment);
    }

    public List<Payment> getAll() {
        return repository.findAll();
    }
}
