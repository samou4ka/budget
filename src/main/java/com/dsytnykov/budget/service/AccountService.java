package com.dsytnykov.budget.service;

import com.dsytnykov.budget.dto.AccountDto;
import com.dsytnykov.budget.model.Account;
import com.dsytnykov.budget.model.Expense;
import com.dsytnykov.budget.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class AccountService {

    private final AccountRepository repository;

    public Account create(AccountDto data) {
        Account account = Account.builder()
                .name(data.name())
                .balance(data.balance())
                .description(data.description())
                .currency(data.currency())
                .build();
        return repository.save(account);
    }

    public AccountService(AccountRepository repository) {
        this.repository = repository;
    }

    public List<Account> getAll() {
        return repository.findAll();
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public Account findById(long id) {
        return repository.findById(id).orElse(new Account());
    }

    public Account update(AccountDto dto) {
        Optional<Account> accountFromDB = repository.findById(dto.id());
        if(accountFromDB.isEmpty()) {
            throw new IllegalArgumentException("Account with id " + dto.id() + " not found!");
        }
        Account account = accountFromDB.get();

        account.setName(dto.name());
        account.setBalance(dto.balance());
        account.setDescription(dto.description());
        account.setCurrency(dto.currency());

        return repository.save(account);
    }

    public Account updateAccountBalance(Expense data) {
        Account account = findById(data.getAccount().getId());
        account.setBalance(account.getBalance() + data.getPrice());
        return repository.save(account);
    }
}
