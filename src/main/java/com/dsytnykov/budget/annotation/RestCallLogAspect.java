package com.dsytnykov.budget.annotation;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Aspect
@Component
@Slf4j
public class RestCallLogAspect {

    @Around("@annotation(RestCallLog)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        Instant initTime = Instant.now();
        Object proceed = joinPoint.proceed();
        Instant endTime = Instant.now();
        log.debug("============================================================================================================");
        log.debug("Method Signature is : {}", joinPoint.getSignature());
        log.debug("Method executed in : {}ms", Duration.between(initTime, endTime).toMillis());
        if(joinPoint.getArgs().length > 0) {
            log.debug("Input Request: {}", joinPoint.getArgs()[0]);
        }
        log.debug("Output Response: {}", proceed);
        return proceed;
    }
}
