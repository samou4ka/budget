package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.PaymentDto;
import com.dsytnykov.budget.model.Payment;
import com.dsytnykov.budget.service.PaymentService;
import lombok.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/payments")
public class PaymentController {

    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping(value = "")
    public Payment create(@NonNull @RequestBody PaymentDto data) {
        return paymentService.create(data);
    }

    @GetMapping(value = "")
    public List<Payment> getAll() {
        return paymentService.getAll();
    }
}
