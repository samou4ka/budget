package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.AccountDto;
import com.dsytnykov.budget.model.Account;
import com.dsytnykov.budget.service.AccountService;
import lombok.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "")
    public Account create(@NonNull  @RequestBody AccountDto data) {
        return accountService.create(data);
    }

    @GetMapping(value = "")
    public List<Account> getAll() {
        return accountService.getAll();
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") long id) {
        accountService.deleteById(id);
    }

    @PutMapping(value = "/{id}")
    public Account update(@NonNull @RequestBody AccountDto data) {
        return accountService.update(data);
    }
}
