package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.ExpenseDto;
import com.dsytnykov.budget.model.Expense;
import com.dsytnykov.budget.service.ExpenseService;
import lombok.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;


@RestController
@RequestMapping("/api/expenses")
public class ExpenseController {
    private final ExpenseService expenseService;

    public ExpenseController(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @GetMapping(value = "")
    public List<Expense> getAll() {
        return expenseService.getAll();
    }

    @PostMapping(value = "")
    public Expense create(@NonNull @RequestBody ExpenseDto data) {
        return expenseService.create(data);
    }

    @PutMapping(value = "", params = "method")
    public Expense update(@NonNull @RequestBody ExpenseDto data, @RequestParam(required = false, defaultValue = "" ) String method) throws NoSuchElementException {
        return expenseService.update(data, method);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") long id) {
        expenseService.deleteById(id);
    }

    @GetMapping(value = "/{id}")
    public Expense findById(@PathVariable("id") long id) {
        return expenseService.findById(id);
    }

    @GetMapping(value = "/category/{id}")
    public List<Expense> findByCategory(@PathVariable("id") long id) {
        return expenseService.findByCategoryId(id);
    }

    @GetMapping(value = "/account/{id}")
    public List<Expense> findByAccount(@PathVariable("id") long id) {
        return expenseService.findByAccountId(id);
    }

}
