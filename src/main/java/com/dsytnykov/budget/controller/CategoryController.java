package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.annotation.RestCallLog;
import com.dsytnykov.budget.dto.CategoryDto;
import com.dsytnykov.budget.model.Category;
import com.dsytnykov.budget.service.CategoryService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping(value = "")
    @RestCallLog
    public Category create(@Valid @RequestBody CategoryDto data) {
        return categoryService.create(data);
    }

    @PutMapping(value = "")
    @RestCallLog
    public Category update(@Valid @RequestBody CategoryDto data) {
        return categoryService.update(data);
    }

    @GetMapping(value = "")
    @RestCallLog
    public List<Category> getAll() {
        return categoryService.getAll();
    }

    @DeleteMapping(value = "/{id}")
    @RestCallLog
    public void delete(@PathVariable("id") long id) {
        categoryService.deleteById(id);
    }

    @GetMapping(value = "/{id}")
    @RestCallLog
    public Category findById(@PathVariable("id") long id) {
        return categoryService.findById(id);
    }
}
