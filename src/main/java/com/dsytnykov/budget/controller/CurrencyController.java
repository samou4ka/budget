package com.dsytnykov.budget.controller;

import com.dsytnykov.budget.dto.CurrencyDto;
import com.dsytnykov.budget.model.Currency;
import com.dsytnykov.budget.service.CurrencyService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/currencies")
public class CurrencyController {

    private final CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping(value = "")
    public List<Currency> getAll() {
        return currencyService.getAll();
    }

    @PostMapping(value = "")
    public Currency create(CurrencyDto currency) {
        return currencyService.create(currency);
    }

    @PutMapping(value = "")
    public Currency update(CurrencyDto currency) {
        return currencyService.update(currency);
    }
}
