package com.dsytnykov.budget.dto;

import lombok.NonNull;

public record CategoryDto(long id, @NonNull String name, Long parentId) {}
