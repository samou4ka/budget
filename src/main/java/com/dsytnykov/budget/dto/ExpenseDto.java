package com.dsytnykov.budget.dto;

import com.dsytnykov.budget.model.Account;
import com.dsytnykov.budget.model.Category;
import com.dsytnykov.budget.model.Currency;
import com.dsytnykov.budget.model.Payment;

public record ExpenseDto(long id, String comment, double price, Currency currency, Category category, Account account, Payment payment) {}
