package com.dsytnykov.budget.dto;

public record CurrencyDto(long id, String symbol, String rate, boolean defaultCurrency) { }
