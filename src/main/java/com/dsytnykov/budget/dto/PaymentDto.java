package com.dsytnykov.budget.dto;

public record PaymentDto(long id, String name) { }
