package com.dsytnykov.budget.dto;

import com.dsytnykov.budget.model.Currency;
import lombok.NonNull;

public record AccountDto(long id, @NonNull String name, double balance, String description, @NonNull Currency currency) {}
