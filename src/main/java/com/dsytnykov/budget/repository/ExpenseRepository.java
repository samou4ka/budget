package com.dsytnykov.budget.repository;

import com.dsytnykov.budget.model.Expense;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {
    List<Expense> findByCategoryId(Long id);

    List <Expense> findByAccountId(Long id);
}
