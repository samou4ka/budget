package com.dsytnykov.budget.model;

public enum CurrencyEnum {
    USD, EUR, UAH
}
